extends Node3D
var square_wall_resource : Resource = preload("res://scene/SquareWall.tscn")
@export var maze_size : int = 2

# Called when the node enters the scene tree for the first time.
func _ready():
	var square_wall : MeshInstance3D = square_wall_resource.instantiate()
	square_wall.position = Vector3(square_wall.mesh.size.x,square_wall.mesh.size.y/2,square_wall.mesh.size.z)
	var maze : Array = []
	maze.resize(maze_size)
	maze.fill([])
	for x in range(maze_size):
		maze[x] = []
		maze[x].resize(maze_size)
		maze[x].fill([])
		for y in range(maze_size):
			maze[x][y] = {}
			maze[x][y].x = x
			maze[x][y].y = y
			maze[x][y].neighbours = {}
			maze[x][y].is_visited = false
			maze[x][y].neighbours.not_visited_count = 0
			var not_visited_count = maze[x][y].neighbours.not_visited_count
			if(x-1 >= 0):
				maze[x][y].neighbours[not_visited_count] = [x-1,y,"has_wall"]
				not_visited_count += 1
			if(y-1 >= 0):
				maze[x][y].neighbours[not_visited_count] = [x,y-1,"has_wall"]
				not_visited_count += 1
			if(y+1 < maze_size):
				maze[x][y].neighbours[not_visited_count] = [x,y+1,"has_wall"]
				not_visited_count += 1
			if(x+1 < maze_size):
				maze[x][y].neighbours[not_visited_count] = [x+1,y,"has_wall"]
				not_visited_count += 1
			maze[x][y].neighbours.not_visited_count = not_visited_count
	
	var selected_cell = maze[0][0]
	selected_cell.is_visited = true
	var random_cell = selected_cell.neighbours[randi() % selected_cell.neighbours.not_visited_count]
	#print(selected_cell.neighbours.size())
	#print(random_cell)
	var current_cell = maze[random_cell[0]][random_cell[1]]
	#print(current_cell)
	if(!current_cell.is_visited):
		pass
		#print(current_cell)
	#print(maze[random_cell[0]][random_cell[1]])
	for cell in maze:
		print(cell)
	add_child(square_wall)
	
