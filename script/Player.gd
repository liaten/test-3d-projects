extends CharacterBody3D

@export var minimal_speed :float = 5.0
@export var speed_exponent: float = 3.0
@export var max_speed : float = 50.0
@export var jump_height :float = 15.5
@export var fall_acceleration :float = 50.0
@export var mouse_sensitivity :float = 0.5

@onready var pivot : Node3D = $CamOrigin
@onready var camera: Camera3D = $CamOrigin/SpringArm3D/Camera3D
@onready var debug_label : Label = $CamOrigin/SpringArm3D/Camera3D/UI/Debug

var target_velocity :Vector3 = Vector3.ZERO
var time_start : float = 0
var time_now : float = 0
var is_timer_on : bool = false
var current_speed := minimal_speed


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _input(event):
	if event is InputEventMouseMotion:
		rotate_y(deg_to_rad(-event.relative.x * mouse_sensitivity))
		pivot.rotate_x(deg_to_rad(-event.relative.y * mouse_sensitivity))
		pivot.rotation.x = clamp(pivot.rotation.x, deg_to_rad(-89), deg_to_rad(0))
		


func check_and_start_timer():
	if(!is_timer_on):
		time_start = Time.get_unix_time_from_system()
		is_timer_on = true


func get_direction() -> Vector3:
	# Получаем направление взгляда камеры
	var forward_direction = -pivot.global_transform.basis.z.normalized()
	var right_direction = pivot.global_transform.basis.x.normalized()
	
	forward_direction.y = 0
	right_direction.y = 0
	
	# Проверяем нажатия клавиш
	var input_direction :Vector3 = Vector3.ZERO
	if Input.is_action_pressed("key_up"):
		input_direction += forward_direction
		check_and_start_timer()
	if Input.is_action_pressed("key_down"):
		input_direction -= forward_direction
		check_and_start_timer()
	if Input.is_action_pressed("key_right"):
		input_direction += right_direction
		check_and_start_timer()
	if Input.is_action_pressed("key_left"):
		input_direction -= right_direction
		check_and_start_timer()
	
	input_direction = input_direction.normalized()
	
	if(is_timer_on):
		time_now = Time.get_unix_time_from_system() - time_start
		if(input_direction == Vector3.ZERO):
			is_timer_on = false
			time_now = 0
	
	debug_label.text += (
			str("forward_direction: ",forward_direction)+
			str("\nright_direction: ",right_direction) +
			str("\ntimer:", time_now) + 
			str("\nis_timer_on:",is_timer_on)
		)
	return input_direction


func _physics_process(delta) -> void:
	debug_label.text = ""
	var direction : Vector3 = get_direction()
	debug_label.text += ("\n"+
		str("x: ",direction.x)+
		str("\nz: ",direction.z)
	)
	if not is_on_floor():
		target_velocity.y = target_velocity.y - (fall_acceleration * delta)
	elif Input.is_action_pressed("space_key"):
		target_velocity.y = jump_height
	
	current_speed = minimal_speed + pow(time_now,speed_exponent)
	if(current_speed>max_speed):
		current_speed = max_speed
	debug_label.text += (
		str("\ncurrent_speed: ", current_speed)
	)
	target_velocity.x = direction.x * current_speed
	target_velocity.z = direction.z * current_speed

	# Moving the Character
	velocity = target_velocity
	move_and_slide()

